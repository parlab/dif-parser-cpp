/* DIF (Dataflow Interchange Format) parser for CPP. */
#pragma once
#ifndef __DIF_PARSER_CPP_HPP__
#define __DIF_PARSER_CPP_HPP__

#include "aux.hpp"
#include <exception>
#include <functional>
#include <iostream>
#include <map>
#include <memory>
#include <optional>
#include <string>
#include <typeinfo>
#include <vector>

namespace DIF {

class Error : std::exception {
  std::string description;

public:
  Error(char const *description) : description(std::string(description)) {}
  const char *what() const noexcept { return description.c_str(); }
};

// Lexer

class Node : Debug {
public:
  std::string const name;
  Node(std::string const name) : name(name) {}
  void debug(int indent);
};

class Edge : Debug {
public:
  std::string const name;
  std::shared_ptr<const Node> node_a;
  std::shared_ptr<const Node> node_b;
  Edge(std::string name, std::shared_ptr<const Node> node_a,
       std::shared_ptr<const Node> node_b)
      : name(name), node_a(node_a), node_b(node_b) {}
  void debug(int indent);
};

class Topology : Debug {
public:
  std::map<std::string const, Node> nodes;
  std::vector<Edge> edges;
  void debug(int indent);
};

class Graph : Debug {
public:
  Topology topology;
  std::string const name;
  std::string const moc;

  Graph(Topology topology, std::string const name, std::string const moc)
      : topology(topology), name(name), moc(moc) {}
  void debug(int indent);
};

class DIFParser {
public:
  virtual std::vector<Graph> parse_dif_file(FILE *file) = 0;
};

} // namespace DIF

#endif
