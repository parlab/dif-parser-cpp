#include "flexbison-dif-parser.hpp"
#include <cstdio>
#include <iostream>

int main() {
  int code;

  auto parser = new FlexBisonDIFParser();

  auto graphs = parser->parse_dif_file(fopen("tests/1.dif", "r"));

  for (auto &graph : graphs) {
    graph.debug(0);
  }
}
